#!/usr/bin/env python

import requests
import datetime
from pathlib import Path

urls_ = dict(
    infrared_1="https://mausam.imd.gov.in/Satellite/3Dasiasec_ir1.jpg",
    visible="https://mausam.imd.gov.in/Satellite/3Dasiasec_vis.jpg",
    water_vapor="https://mausam.imd.gov.in/Satellite/3Dasiasec_wv.jpg",
    cloud_top_brightness_temp="https://mausam.imd.gov.in/Satellite/3Dasiasec_ctbt.jpg",
    # infrared_1_gif="https://mausam.imd.gov.in/Satellite/Converted/IR1.gif",
    # visible_gif="https://mausam.imd.gov.in/Satellite/Converted/VIS.gif",
    # water_vapor_gif="https://mausam.imd.gov.in/Satellite/Converted/WV.gif",
)


def main():
    basepath = Path(__file__).parent / datetime.datetime.now().strftime('%Y%m%d/%H%M')
    basepath.mkdir(parents=True, exist_ok=True)
    for key, url in urls_.items():
        # print(f"[INFO] download {key}")
        r = requests.get(url, timeout=2)
        if r.status_code == 200:
            print(r.status_code)
            path = basepath / url.split("/")[-1]
            with open(path, "wb") as f:
                f.write(r.content)
                print(f"[INFO] Saved to {path}")

if __name__ == "__main__":
    main()
